export class Setting {
  constructor(
    public id: number,
    public name: string,
    public value: string,
    public isCrypt: boolean
  ) {
  }
}
