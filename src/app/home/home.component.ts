import { Component, OnInit } from '@angular/core';
import { Account } from '../data/Account';
import { AccountService } from '../service/account/account.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  account: Account;
  roles = [];
  isAdmin = false;
  accesDSLPR = false;
  accesCO2 = false;
  accesMonitoring = false;
  accessTopAlliance = false;

  constructor(private accountService: AccountService, public translate: TranslateService) {
  }

  ngOnInit() {
    this.getAccount();
  }

  /**
   * get user's information
   */
  getAccount() {
    this.accountService.getAccount().subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }

  private handleSuccessfulResponse(response) {

    if (response) {
      this.account = response;
      this.roles = this.account.roles;

      //show or hide the link
      this.isAdmin = this.roles.indexOf('ADMIN') >= 0 || this.isAdmin ? true : false;
      this.accesDSLPR = this.roles.indexOf('DSLPR') >= 0 || this.isAdmin ? true : false;
      this.accesCO2 = this.roles.indexOf('CO2') >= 0 || this.isAdmin ? true : false;
      this.accesMonitoring = this.roles.indexOf('MONITORING') >= 0 || this.isAdmin ? true : false;
      this.accessTopAlliance = this.roles.indexOf('TOP_ALLIANCE') >= 0 || this.isAdmin ? true : false;
    }
  }

}
