import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';

@Component({
  selector: 'app-oidc',
  templateUrl: './oidc.component.html',
  styleUrls: ['./oidc.component.css']
})
export class OidcComponent implements OnInit {

  constructor(private oauthService: OAuthService, private router: Router) {
    router.navigate(['/home']);
  }

  ngOnInit() {
  }

}
