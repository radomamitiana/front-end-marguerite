import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
    /*issuer: environment.issuer,
    redirectUri: environment.redirectUri,
    clientId: environment.clientId,
    scope: environment.scope*/
	
    /*issuer: "https://idp2.renault.com/nidp/oauth/nam",
    loginUrl: "https://idp2.renault.com/nidp/oauth/nam/authz",
    logoutUrl: "https://idp2.renault.com/nidp/app/logout",
    clientId: "2179f5ac-a1a7-4d15-a1ab-304418730b10",
    scope: "openid arca dsb-dev-irn70295",
    redirectUri: "https://dsb.dev1.applis.renault.fr/open-id-callback"*/
	issuer: "https://idp2.renault.com/nidp/oauth/nam",
    loginUrl: "https://idp2.renault.com/nidp/oauth/nam/authz",
    logoutUrl: "https://idp2.renault.com/nidp/app/logout",
    clientId: "2179f5ac-a1a7-4d15-a1ab-304418730b10",
    scope: "DSB DEV IRN70295",
    redirectUri: "https://dsb.dev1.applis.renault.fr/open-id-callback"

}