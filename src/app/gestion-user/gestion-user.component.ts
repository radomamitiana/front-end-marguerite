import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TranslateService} from '@ngx-translate/core';
import {User} from '../data/User';
import {UserService} from '../service/user/user.service';

@Component({
  selector: 'app-gestion-user',
  templateUrl: './gestion-user.component.html',
  styleUrls: ['./gestion-user.component.css']
})

export class GestionUserComponent implements OnInit {
  public modalRef: BsModalRef;

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  dropdownDataDTOS: {};
  strRoles = [];

  users: User[];
  ipn = '';
  idEdit = -1;
  userId = -1;
  saveUserHasError = false;
  isSuccess = false;
  message = '';
  buttonEditId = -1;
  stringQuery: null;

  totalPages: 1;
  totalPagesArray = [];
  last: false;
  first: false;
  page = 0;

  constructor(private modalService: BsModalService,
              private userService: UserService, public translate: TranslateService) {

  }

  ngOnInit() {
    this.getUsers(this.page);

    // Initialiser les listes des rôles
    this.dropdownList = [
      {id: 1, value: 'ADMIN'},
      {id: 2, value: 'CO2'},
      {id: 3, value: 'TOP_ALLIANCE'},
      {id: 4, value: 'DSLPR'},
      {id: 5, value: 'MONITORING'}
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'value',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    };
  }

  onItemSelect(item: any, userId, selectedItems) {
    this.idEdit = userId;
    this.dropdownDataDTOS = selectedItems;
    this.updateUser();
  }

  onSelectAll(items: any, userId, selectedItems) {
    this.idEdit = userId;
    this.dropdownDataDTOS = selectedItems;
    this.updateUser();
  }

  onItemSelectAdd(item: any) {
    this.strRoles = this.strRoles.concat([item.value]);
  }

  onSelectAllAdd(items: any) {
    this.strRoles = [];
    this.strRoles.concat(['ADMIN', 'CO2', 'TOP_ALLIANCE', 'DSLPR', 'MONITORING']);
  }

  /**
   * Enable or desable button edit
   */
  public onEnableButonEdit(userId): boolean {
    return (this.buttonEditId !== userId);
  }


  /**
   * Open a modal to add an user
   */
  public openAddUserModal(templateAddUser: TemplateRef<any>) {
    this.modalRef = this.modalService.show(templateAddUser);
  }

  /**
   * Open a modal to delete an user
   */
  public openDeleteUserModal(userId, templateDeleteUser: TemplateRef<any>) {
    this.userId = userId;
    this.modalRef = this.modalService.show(templateDeleteUser);
  }

  /**
   * get all user
   */
  getUsers(page) {
    this.userService.getUsers(page).subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }

  /**
   * check response
   * @param response
   */
  private handleSuccessfulResponse(response) {
    if (response && response.userDTOs && response.userDTOs.content) {

      this.users = response.userDTOs.content;
      this.totalPages = response.userDTOs.totalPages;
      this.last = response.userDTOs.last;
      this.first = response.userDTOs.first;
      this.totalPagesArray = [];
      for (let i = 0; i < this.totalPages; i++) {
        this.totalPagesArray[i] = (i + 1);
      }
    }
  }

  goToPage(page: number) {
    this.page = page - 1;
    if (this.stringQuery) {
      this.searchUsers();
    } else {
      this.getUsers(this.page);
    }
  }

  previousPage() {
    this.page = this.page - 1;
    if (this.stringQuery) {
      this.searchUsers();
    } else {
      this.getUsers(this.page);
    }
  }

  nextPage() {
    this.page = this.page + 1;
    if (this.stringQuery) {
      this.searchUsers();
    } else {
      this.getUsers(this.page);
    }
  }

  /**
   * add an user
   */
  addUser() {
    (this.userService.addUser(this.ipn, this.strRoles).subscribe(
        data => {
          this.modalRef.hide();
          this.getUsers(0);
          this.saveUserHasError = false;
          this.isSuccess = true;
          this.translate.get('notification.add').subscribe((text: string) => {
            this.message = text;
          });
          this.strRoles = [];
          this.clearForm();
        },
        error => {
          this.saveUserHasError = true;
          this.isSuccess = false;
          this.strRoles = [];
          this.clearForm();
        }
      )
    );
  }

  /**
   * edit user's role
   */
  updateUser() {
    (this.userService.updateUser(this.idEdit, this.dropdownDataDTOS).subscribe(
        data => {
          this.modalRef.hide();
          this.getUsers(0);
          this.saveUserHasError = false;
          this.isSuccess = true;
          this.translate.get('notification.edit').subscribe((text: string) => {
            this.message = text;
          });
          this.strRoles = [];
        },
        error => {
          this.saveUserHasError = true;
          this.strRoles = [];
          this.isSuccess = false;
        }
      )
    );

  }

  /**
   * delete user
   */
  deleteUser() {
    (this.userService.deleteUser(this.userId).subscribe(
        data => {
          this.modalRef.hide();
          this.getUsers(0);
          this.saveUserHasError = false;
          this.userId = -1;
          this.isSuccess = true;
          this.translate.get('notification.delete').subscribe((text: string) => {
            this.message = text;
          });
        },
        error => {
          this.saveUserHasError = true;
          this.userId = -1;
          this.isSuccess = false;
        }
      )
    );
  }

  /**
   * search something in the table
   */
  searchUsers() {
    this.userService.searchUsers(this.stringQuery, this.page).subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }

  clearForm() {
    this.ipn = '';
    this.selectedItems = [];
  }

}
