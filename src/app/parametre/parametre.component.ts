import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TranslateService} from '@ngx-translate/core';
import {SettingService} from '../service/setting/setting.service';
import {Setting} from '../data/Setting';

@Component({
  selector: 'app-parametre',
  templateUrl: './parametre.component.html',
  styleUrls: ['./parametre.component.css']
})
export class ParametreComponent implements OnInit {
  public modalRef: BsModalRef;
  public settings: Setting[];

  message = '';

  idDeleted: -1;

  id: null;
  nameAdd: '';
  valueAdd: '';
  isCryptAdd: false;

  nameEdit: '';
  valueEdit: '';
  isCryptEdit: false;

  isSuccess = false;

  stringQuery: null;

  pageEmpty: true;
  totalPages: 1;
  totalPagesArray = [];
  last: false;
  first: false;
  page = 0;

  constructor(private modalService: BsModalService, private settingService: SettingService, public translate: TranslateService) {
  }

  ngOnInit() {
    this.clearData();
    if (this.stringQuery) {
      this.searchSettings();
    } else {
      this.getSettings(this.page);
    }
  }

  /**
   * Open a modal to add an setting
   */
  public openAddParameterModal(templateAddParameter: TemplateRef<any>) {
    this.modalRef = this.modalService.show(templateAddParameter);
  }

  /**
   * Open a modal to edit an setting
   */
  public openEditParameterModal(templateEditParameter: TemplateRef<any>, id, name, value, isCrypt) {

    this.id = id;
    this.nameEdit = name;
    this.valueEdit = value;
    this.isCryptEdit = isCrypt;

    this.modalRef = this.modalService.show(templateEditParameter);
  }

  /**
   * Open a modal to add an setting
   */
  public openDeleteParameterModal(templateDeleteParameter: TemplateRef<any>, id) {
    this.idDeleted = id;
    this.modalRef = this.modalService.show(templateDeleteParameter);
  }

  /**
   * get all setting
   */
  getSettings(page) {
    this.settingService.getSettings(page).subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }

  /**
   * check response
   * @param response
   */
  private handleSuccessfulResponse(response) {
    if (response && response.settingDTOS && response.settingDTOS.content) {
      console.log(response);
      this.settings = response.settingDTOS.content;
      this.pageEmpty = response.settingDTOS.empty;
      this.totalPages = response.settingDTOS.totalPages;
      this.last = response.settingDTOS.last;
      this.first = response.settingDTOS.first;
      this.totalPagesArray = [];
      for (let i = 0; i < this.totalPages; i++) {
        this.totalPagesArray[i] = (i + 1);
      }
    }
  }

  goToPage(page: number) {
    this.page = page - 1;
    if (this.stringQuery) {
      this.searchSettings();
    } else {
      this.getSettings(this.page);
    }
  }

  previousPage() {
    this.page = this.page - 1;
    if (this.stringQuery) {
      this.searchSettings();
    } else {
      this.getSettings(this.page);
    }
  }

  nextPage() {
    this.page = this.page + 1;
    if (this.stringQuery) {
      this.searchSettings();
    } else {
      this.getSettings(this.page);
    }
  }

  /**
   * add an setting
   */
  createOrUpdateSetting(isCreate: boolean) {
    if (isCreate) {
      (this.settingService.createOrUpdateSetting(null, this.nameAdd, this.valueAdd, this.isCryptAdd).subscribe(
          data => {
            this.isSuccess = true;
            this.translate.get('notification.add').subscribe((text: string) => {
              this.message = text;
            });
            this.getSettings(0);
          },
          error => {
            this.isSuccess = false;
          }
        )
      );
      this.clearData();
    } else {
      (this.settingService.createOrUpdateSetting(this.id, this.nameEdit, this.valueEdit, this.isCryptEdit).subscribe(
          data => {
            this.isSuccess = true;
            this.translate.get('notification.edit').subscribe((text: string) => {
              this.message = text;
            });
            this.getSettings(0);
          },
          error => {
            this.isSuccess = false;
          }
        )
      );
    }
    this.modalRef.hide();
  }

  clearData() {
    this.nameAdd = '';
    this.valueAdd = '';
    this.isCryptAdd = false;
  }

  /**
   * delete setting
   */
  deleteSetting() {
    (this.settingService.deleteSetting(this.idDeleted).subscribe(
        data => {
          this.modalRef.hide();
          this.getSettings(0);
          this.idDeleted = -1;
          this.isSuccess = true;
          this.translate.get('notification.delete').subscribe((text: string) => {
            this.message = text;
          });
        },
        error => {
          this.idDeleted = -1;
          this.isSuccess = false;
        }
      )
    );
  }

  /**
   * search something in the table
   */
  searchSettings() {
    this.settingService.searchSettings(this.stringQuery, this.page).subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }
}
