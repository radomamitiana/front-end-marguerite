import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { GestionUserComponent } from './gestion-user/gestion-user.component';
import { HomeComponent } from './home/home.component';
import { AuthGaurdService } from './service/security/auth-gaurd.service';
import { LogoutComponent } from './logout/logout.component';
import { DslprComponent } from './dslpr/dslpr.component';
import { Co2Component } from './co2/co2.component';
import { TopAllianceComponent } from './top-alliance/top-alliance.component';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { ParametreComponent } from './parametre/parametre.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGaurdService] },
  { path: 'login', component: AuthComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'users', component: GestionUserComponent, canActivate: [AuthGaurdService] },
  { path: 'dslpr', component: DslprComponent, canActivate: [AuthGaurdService] },
  { path: 'co2', component: Co2Component, canActivate: [AuthGaurdService] },
  { path: 'top_alliance', component: TopAllianceComponent, canActivate: [AuthGaurdService] },
  { path: 'monitoring', component: MonitoringComponent, canActivate: [AuthGaurdService] },
  { path: 'parameter', component: ParametreComponent, canActivate: [AuthGaurdService] },
  { path: '', component: HomeComponent, canActivate: [AuthGaurdService] },
  {path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
