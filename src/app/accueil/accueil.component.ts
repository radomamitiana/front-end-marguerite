import {Component, OnInit} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {Account} from '../data/Account';
import {AccountService} from '../service/account/account.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  account: Account;
  roles = [];
  isAdmin = false;
  accesDSLPR = false;
  accesCO2 = false;
  accesMonitoring = false;
  accessTopAlliance = false;

  constructor(private accountService: AccountService, public translate: TranslateService) {
  }

  ngOnInit() {
    this.getAccount();
  }

  getAccount() {
    this.accountService.getAccount().subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }

  private handleSuccessfulResponse(response) {

    if (response) {
      this.account = response;
      this.roles = this.account.roles;
      // Show and hide the link url
      this.isAdmin = this.roles.indexOf('ADMIN') >= 0 ? true : false;
      this.accesDSLPR = this.roles.indexOf('DSLPR') >= 0 || this.isAdmin ? true : false;
      this.accesCO2 = this.roles.indexOf('CO2') >= 0 || this.isAdmin ? true : false;
      this.accesMonitoring = this.roles.indexOf('MONITORING') >= 0 || this.isAdmin ? true : false;
      this.accessTopAlliance = this.roles.indexOf('TOP_ALLIANCE') >= 0 || this.isAdmin ? true : false;
    }
  }

  private switchingLanguage(language: string) {
    this.translate.use(language);
  }
}
