import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const API_URL = window.location.protocol + '//' + window.location.host + "/api/";


const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

@Injectable()
export class PostService {

    constructor(private _http: HttpClient) { }

    private formData: FormData;


    public uploadFile(fileToUpload: File, uploadFileUrl: string, data: any): Observable<any> {
        this.formData = new FormData();        
        this.formData.append('file', fileToUpload, fileToUpload.name);
        
        return this._http.post(API_URL+uploadFileUrl+"?types="+data.types+"&commemtaire="+data.commemtaire+"&ipn="+data.ipn, this.formData);
    }

    public postRequest(postUrl: string, data: any) : Observable<any>{
        return this._http.post(API_URL+postUrl, data, httpOptions);
    }

}