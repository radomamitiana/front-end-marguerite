import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../../data/User';

const API_URL = window.location.protocol + '//' + window.location.host + "/api/";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * get all user
   */
  getUsers(page) {
    const headers = new HttpHeaders({Authorization: 'Bearer ' + sessionStorage.getItem('accessToken')});
    return this.httpClient.get<User[]>(API_URL + '/user/list?page=' + page, {headers});
  }

  /**
   * add an user
   * @param ipn
   * @param strRoles
   */
  addUser(ipn, strRoles) {
    const headers = new HttpHeaders({Authorization: 'Bearer ' + sessionStorage.getItem('accessToken')});
    return this.httpClient.post<User>(API_URL + '/user/add', {ipn, strRoles}, {headers});
  }

  /**
   * update user's role
   * @param id
   * @param dropdownDataDTOS
   */
  updateUser(id, dropdownDataDTOS) {
    const headers = new HttpHeaders({Authorization: 'Bearer ' + sessionStorage.getItem('accessToken')});
    return this.httpClient.put<User>(API_URL + '/user/update', {id, dropdownDataDTOS}, {headers});
  }

  /**
   * delete user
   * @param id
   */
  deleteUser(id) {
    const headers = new HttpHeaders({Authorization: 'Bearer ' + sessionStorage.getItem('accessToken')});
    return this.httpClient.delete<User>(API_URL + '/user/delete?id=' + id, {headers});
  }

  /**
   * search  user
   * @param stringQuery
   * @param page
   */
  searchUsers(stringQuery, page) {
    const headers = new HttpHeaders({Authorization: 'Bearer ' + sessionStorage.getItem('accessToken')});
    return this.httpClient.get<User[]>(API_URL + '/user/list?stringQuery=' + stringQuery + '&page=' + page, {headers});
  }
}
