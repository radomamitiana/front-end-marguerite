import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Account } from '../../data/Account';

const API_URL = window.location.protocol + '//' + window.location.host + "/api/";

@Injectable({
  providedIn: 'root'
})
export class AccountService {


  constructor(private httpClient: HttpClient) {
  }

  /**
   * get users's information
   */
  getAccount() {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.get<Account>(API_URL + '/account/get', { headers });
  }
}
