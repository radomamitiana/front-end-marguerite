import { HttpClient, HttpEvent, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {History} from '../../data/History';

const API_URL = window.location.protocol + '//' + window.location.host + "/api/";

@Injectable({
  providedIn: 'root'
})
export class DownloadService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Download Cause ALE Deviation file from datalake to local
   */
  
   downloadCauseALEDeviation(): Observable<HttpResponse<String>> {
    const httpHeaders = new HttpHeaders({
      Authorization: 'Bearer ' + sessionStorage.getItem('accessToken'),
      //Accept: 'text/csv; charset=utf-8'
    });

    return this.httpClient.get(API_URL + '/dslpr/downloadCauseAleDeviation', {
      headers: httpHeaders,
      observe: 'response',
      responseType: 'text'
    });
  }


  /**
   * Download Error report
   */
  
  onDownloadErrorReport(errorReport: string): Observable<HttpResponse<String>> {
    const httpHeaders = new HttpHeaders({
      Authorization: 'Bearer ' + sessionStorage.getItem('accessToken'),
      //Accept: 'text/csv; charset=utf-8'
    });

    return this.httpClient.get(API_URL + '/dslpr/downloadErrorReport/'+errorReport, {
      headers: httpHeaders,
      observe: 'response',
      responseType: 'text'
    });
  }

  /**
   * upload file from local to the datalake
   * @param file 
   */
  uploadCauseALEDeviation(file: File): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    const httpHeaders = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    const req = new HttpRequest('POST', API_URL + '/dslpr/uploadCauseAleDeviation', formdata, {
      headers: httpHeaders,
      reportProgress: true,
      responseType: 'text'
    });

    return this.httpClient.request(req);
  }

  /**
   * get all history
   */
  getHistorys(page) {
    const headers = new HttpHeaders({Authorization: 'Bearer ' + sessionStorage.getItem('accessToken')});
    return this.httpClient.get<History[]>(API_URL + '/history/list?page=' + page, {headers});
  }

  /**
   * search  history
   * @param stringQuery
   */
  searchHistorys(stringQuery, page) {
    const headers = new HttpHeaders({Authorization: 'Bearer ' + sessionStorage.getItem('accessToken')});
    return this.httpClient.get<History[]>(API_URL + '/history/list?stringQuery=' + stringQuery + '&page=' + page, {headers});
  }

}
