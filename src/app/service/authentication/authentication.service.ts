import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

const API_URL = window.location.protocol + '//' + window.location.host + "/api/";

export class User {
  constructor(
    public status: string,
  ) {
  }

}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

/**
 * chech if a session exist for this user
 */
  public isUserLoggedIn() {
    const user = sessionStorage.getItem('ipn');
    return !(user === null);
  }

  /**
   * user's authentification
   * @param ipn 
   * @param password 
   */
  authenticate(ipn, password) {
    return this.httpClient.post<any>(API_URL + '/auth/signin', { ipn, password }).pipe(
      map(
        userData => {
          sessionStorage.setItem('ipn', ipn);
          const tokenStr = 'Bearer ' + userData.accessToken;
          sessionStorage.setItem('token', tokenStr);
          sessionStorage.setItem('accessToken', userData.accessToken);
          return userData;
        }
      )
    );
  }

  logOut() {
    sessionStorage.removeItem('ipn');
  }
}
