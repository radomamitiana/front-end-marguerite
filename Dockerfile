FROM node:latest as builder

WORKDIR /usr/src
COPY . .

RUN npm config set proxy $HTTP_PROXY &&\
  npm install -g @angular/cli --ignore-scripts
RUN cd src &&\
  npm install --ignore-scripts &&\
  ng build

FROM httpd:latest

LABEL SIA="dsb"
LABEL IRN="70295"
LABEL PROJECT_NAME="dsb"
LABEL MAINTAINER="rado.herilalao-extern@renault.com"

ENV TERM=xterm
ENV SIA="dsb"
ENV IRN="70295"
ENV APP_NAME="dsb-app"

COPY Dockerfile .

RUN apt-get update && apt-get install -y curl \
&& rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/src/dist/front-end/ /usr/local/apache2/htdocs/
